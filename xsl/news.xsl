<?xml version="1.0" encoding="us-ascii"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		version="1.0">
  <xsl:import href="common.xsl" />

  <xsl:output method="xml" encoding="windows-1251" indent="yes" />

  <xsl:template match="/">
    <xsl:apply-templates select="devel" />
  </xsl:template>

  <xsl:template match="/devel">
    <rss version="2.0">
      <xsl:comment>
	<xsl:value-of select="concat(' Ringlet website rev. ', $vcs-id, ' ', $vcs-date, ' ')" />
      </xsl:comment>
      
      <channel>
	<title>devel.ringlet.net News</title>
	<link>http://devel.ringlet.net/</link>
	<description>News about the devel.ringlet.net software projects</description>

      <xsl:apply-templates select="news/*/*/*/event" />

      </channel>
    </rss>
  </xsl:template>

  <xsl:template match="/devel/news//event">
    <item>
      <title>
	<xsl:value-of select="concat(../@name, ' ', ../../@name, ' ',
	  ../../../@name, ': ', @id)" />
      </title>
      <xsl:if test="@pubDate">
	<pubDate><xsl:value-of select="@pubDate" /></pubDate>
      </xsl:if>
      <xsl:if test="not(@pubDate)">
	<xsl:message terminate="no">
	  <xsl:value-of select='concat("News item with no publication date: ",
	    ../../../@name, "-", ../../@id, "-", ../@name, "-", @id)' />
	</xsl:message>
      </xsl:if>
      <link>
	<xsl:value-of select="concat('http://devel.ringlet.net/index.html#',
	  'event-', ../../../@name, '-', ../../@id, '-', ../@name,
	  '-', @id)" />
      </link>
      <description><xsl:apply-templates /></description>
    </item>
  </xsl:template>

  <xsl:template match="news//event/release">
    <xsl:call-template name="ringlet-news-event-release-rss" />
  </xsl:template>
</xsl:stylesheet>
