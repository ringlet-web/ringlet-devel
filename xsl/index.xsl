<?xml version="1.0" encoding="us-ascii"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:import href="common.xsl" />

  <xsl:output method="xml" encoding="us-ascii" indent="yes"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />

  <xsl:template match="/">
    <xsl:apply-templates select="devel" />
  </xsl:template>

  <xsl:template match="/devel">
    <html>
      <head>
	<title>Welcome to devel.ringlet.net!</title>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<style type="text/css">
	  body {
	    margin: 40px auto;
	    line-height: 1.6;
	    font-size: 18px;
	    color: #444;
	    padding: 0 10px
	  }
	  h1, h2, h3 {
	    line-height: 1.2
	  }
	</style>
      </head>
      <body>
	<xsl:comment>
	  <xsl:value-of select="concat(' Ringlet website rev. ', $vcs-id, ' ', $vcs-date, ' ')" />
	</xsl:comment>

	<table border="0" cellpadding="1" cellspacing="0">
	  <tr>
	    <td align="left" valign="top">
	      <h2>Welcome to devel.ringlet.net!</h2>
      
	      <p>This is <a href="http://www.ringlet.net/~roam/">Peter
		Pentchev</a>'s generic project repository.</p>
      
	      <xsl:apply-templates select="categories" />
      
	      <xsl:for-each select="feedback">
		<xsl:call-template name="ringlet-feedback" />
	      </xsl:for-each>
      
	      <xsl:for-each select="validate">
		<xsl:call-template name="ringlet-validate" />
	      </xsl:for-each>
	    </td>
    
	    <td width="30%" align="left" valign="top">
	      <table border="1" cellpadding="3" cellspacing="0">
		<tr>
		  <td bgcolor="#ffcc66">
		    <xsl:apply-templates select="news" />
		  </td>
		</tr>
	      </table>
	    </td>
	  </tr>
	</table>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="devel/categories">
    <p>Projects hosted here (by category):</p>

    <ul>
      <xsl:for-each select="category">
	<li>
	  <xsl:element name="a">
	    <xsl:attribute name="href">
	      <xsl:value-of select="concat(@name, '/')" />
	    </xsl:attribute>
	    <xsl:value-of select="@desc" />
	  </xsl:element>

	  <ul>
	    <xsl:for-each select="projects/project">
	      <li>
		<xsl:element name="a">
		  <xsl:attribute name="href">
		    <xsl:value-of select="concat(../../@name, '/', @name, '/')" />
		  </xsl:attribute>
		  <xsl:value-of select="@name" />
		</xsl:element>
		<xsl:value-of select="concat(' -- ', @desc)" />
	      </li>
	    </xsl:for-each>
	  </ul>
	</li>
      </xsl:for-each>
    </ul>
  </xsl:template>

  <xsl:template match="devel/feedback">
    <xsl:call-template name="ringlet-feedback" />
  </xsl:template>

  <xsl:template match="devel/news">
    <a name="news" />

    <h3>What's new</h3>

    <p>(also available as <a href="news.rss">a RSS newsfeed</a>)</p>
    
    <xsl:for-each select="year">
      <xsl:for-each select="month">
	<h4><xsl:value-of select="concat(@name, ' ', ../@name)" /></h4>

	<ul>
	  <xsl:for-each select="day">
	    <xsl:for-each select="event">
	      <li>
		<p>
		  <xsl:element name="a">
		    <xsl:attribute name="name">
		      <xsl:call-template name="ringlet-news-event-id" />
		    </xsl:attribute>
		  </xsl:element>
		  <strong>
		    <font size="-1">
		    <xsl:value-of select='concat(../@name, " ", ../../@name,
		      " ", ../../../@name, ": ")' />
		    </font>
		  </strong>
		  
		  <font size="-1"><xsl:apply-templates /></font>
		</p>
	      </li>
	    </xsl:for-each>
	  </xsl:for-each>
	</ul>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="news//event/release">
    <xsl:call-template name="ringlet-news-event-release" />
  </xsl:template>
</xsl:stylesheet>
