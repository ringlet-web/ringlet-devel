<?xml version="1.0" encoding="us-ascii"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:import href="common.xsl" />

  <xsl:output method="xml" encoding="us-ascii" indent="yes"
    doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />

  <xsl:variable name="base">
    <xsl:text>http://devel.ringlet.net/</xsl:text>
  </xsl:variable>

  <xsl:template match="/">
    <xsl:apply-templates select="devel" />
  </xsl:template>

  <xsl:template match="/devel">
    <html>
      <head>
	<title>
	  <xsl:value-of select="$desc" />
	  <xsl:text> -- </xsl:text>
	  <xsl:value-of select="$dirname" />
	  <xsl:text> on devel.ringlet.net</xsl:text>
	</title>
      </head>
      <body>
	<h1>
	  <xsl:value-of select="$desc" />
	  <xsl:text> -- </xsl:text>
	  <xsl:value-of select="$dirname" />
	  <xsl:text> on devel.ringlet.net</xsl:text>
	</h1>

	<xsl:comment>
	  <xsl:value-of select="concat(' Ringlet website rev. ', $vcs-id, ' ', $vcs-date, ' ')" />
	</xsl:comment>

	<xsl:apply-templates select="categories/category[@name=$dirname]" />
      
	<xsl:for-each select="feedback">
	  <xsl:call-template name="ringlet-feedback" />
	</xsl:for-each>
      
	<xsl:for-each select="validate">
	  <xsl:call-template name="ringlet-validate" />
	</xsl:for-each>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="categories/category">
    <ul>
      <xsl:for-each select="projects/project">
	<li>
	  <xsl:element name="a">
	    <xsl:attribute name="href">
	      <xsl:value-of select="concat($base, $dirname, '/', @name, '/')" />
	    </xsl:attribute>
	    <xsl:value-of select="@name" />
	  </xsl:element>
	  <xsl:text> -- </xsl:text>
	  <xsl:value-of select="@desc" />
	</li>
      </xsl:for-each>
    </ul>
  </xsl:template>

  <xsl:template match="devel/feedback">
    <xsl:call-template name="ringlet-feedback" />
  </xsl:template>
</xsl:stylesheet>
