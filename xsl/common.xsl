<?xml version="1.0" encoding="us-ascii"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template name="ringlet-feedback">
    <br />
    <hr noshade="noshade" />

    <address>
      Comments:
      <xsl:apply-templates />
    </address>
  </xsl:template>

  <xsl:template match="feedback/email">
    <xsl:choose>
      <xsl:when test="not(@address) and not(@name)">
	<a href="mailto:roam@ringlet.net">Peter Pentchev</a>
      </xsl:when>
      <xsl:otherwise>
	<xsl:element name="a">
	  <xsl:attribute name="href">
	    <xsl:value-of select='concat("mailto:", @address)' />
	  </xsl:attribute>
	  <xsl:value-of select="@name" />
	</xsl:element>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="ringlet-validate">
    <p>
      <xsl:apply-templates />
    </p>
  </xsl:template>

  <xsl:template match="validate/xhtml">
    <a href="http://validator.w3.org/check/referer"><img
      src="/images/valid-xhtml10.png"
      alt="Valid XHTML 1.0!" height="31" width="88" border="0" /></a>
  </xsl:template>

  <xsl:template name="ringlet-news-event-id">
    <xsl:value-of select='concat("event-", ../../../@name, "-", ../../@id,
      "-", ../@name, "-", @id)' />
  </xsl:template>

  <xsl:template name="ringlet-news-event-release">
    <xsl:value-of select="concat('Version ', @version, ' of the ')" />
    <xsl:element name="a">
      <xsl:attribute name="href">
	<xsl:value-of select='concat(@category, "/", @name, "/")' />
      </xsl:attribute>
      <xsl:value-of select="@name" />
    </xsl:element>
    <xsl:if test="not(@type)">
      <xsl:text> utility</xsl:text>
    </xsl:if>
    <xsl:if test="@type">
      <xsl:value-of select='concat(" ", @type)' />
    </xsl:if>
    <xsl:text> was released (category </xsl:text>
    <xsl:element name="a">
      <xsl:attribute name="href">
	<xsl:value-of select='concat(@category, "/")' />
      </xsl:attribute>
      <xsl:value-of select="@category" />
    </xsl:element>
    <xsl:text>)</xsl:text>
  </xsl:template>

  <xsl:template name="ringlet-news-event-release-rss">
    <xsl:value-of select="concat('Version ', @version,
      ' of the &lt;a href=&quot;', @category, '/', @name, '/&quot;&gt;',
      @name, '&lt;/a&gt;')" />
    <xsl:if test="not(@type)">
      <xsl:text> utility</xsl:text>
    </xsl:if>
    <xsl:if test="@type">
      <xsl:value-of select='concat(" ", @type)' />
    </xsl:if>
    <xsl:value-of select="concat(' was released (category &lt;a href=&quot;',
      @category, '/&quot;&gt;', @category, '&lt;/a&gt;).')" />
  </xsl:template>
</xsl:stylesheet>
