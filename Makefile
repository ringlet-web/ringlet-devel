XMLINDEX=	index.xml
HTMLINDEX=	index.html
RSSNEWS=	news.rss
CAT_SH=		categories.sh

DATA=		${HTMLINDEX} ${RSSNEWS}
CLEANFILES=	${HTMLINDEX} ${RSSNEWS} ${CAT_SH} category-*.html

FALSE?=		false
RM?=		rm -f
SH?=		sh
XSLTPROC?=	xsltproc
XSLTARGS?=	-nonet

XSLTID=		--stringparam vcs-id "`git show --pretty=format:%h HEAD | head -1`" \
		--stringparam vcs-date "`git show --pretty=format:%ai HEAD | head -1`"

STYLESHEET_COMMON=	xsl/common.xsl
STYLESHEET_CAT_SH=	xsl/categories-sh.xsl
STYLESHEET_CAT_INDEX=	xsl/category-index.xsl
STYLESHEET_INDEX=	xsl/index.xsl
STYLESHEET_NEWS=	xsl/news.xsl

all:		${DATA}

clean:
		${RM} ${CLEANFILES}

${HTMLINDEX}:	${XMLINDEX} ${STYLESHEET_INDEX} ${STYLESHEET_COMMON} ${CAT_SH} ${STYLESHEET_CAT_INDEX} ${STYLESHEET_COMMON}
		${SH} ${CAT_SH}
		${XSLTPROC} ${XSLTARGS} ${XSLTID} ${STYLESHEET_INDEX} ${XMLINDEX} > $@ || (${RM} $@; ${FALSE})

${RSSNEWS}:	${XMLINDEX} ${STYLESHEET_NEWS} ${STYLESHEET_COMMON}
		${XSLTPROC} ${XSLTARGS} ${XSLTID} ${STYLESHEET_NEWS} ${XMLINDEX} > $@ || (${RM} $@; ${FALSE})

${CAT_SH}:	${XMLINDEX} ${STYLESHEET_CAT_SH}
		${XSLTPROC} ${XSLTARGS} ${XSLTID} ${STYLESHEET_CAT_SH} ${XMLINDEX} > $@ || (${RM} $@; ${FALSE})
